import { Component, OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { Audit, User } from '@/_models';
import { AuditService, AuthenticationService } from '@/_services';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

export interface UserData {
    _id: string;
    name: string;
    progress: string;
    color: string;
}

@Component({
    templateUrl: 'audit.component.html'
})
export class AuditComponent implements OnInit {
    currentUser: User;
    displayedColumns: string[] = ['user', '_id', 'loginTime', 'logoutTime', 'ip'];
    dataSource: MatTableDataSource<any>;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    audits = [];

    constructor(private authenticationService: AuthenticationService, private auditService: AuditService) {
        this.currentUser = this.authenticationService.currentUserValue;
    }

    ngOnInit() {
        this.loadAllAudits();
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    loadAllAudits() {
        this.auditService.getAll()
            .pipe(first())
            .subscribe((audits) => {
                this.dataSource = new MatTableDataSource(audits);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;

            });
    }

}